/*
  TODO: Generate cloud-data from rec'd json freq data, store that in session
  item "cloud-data". In buildWordCloud, we go through the freqJson, get percBet
  for each item between low and high count and make sure perc is less than high-perc 
  and more than low-perc.
*/
BUCKET_COUNT = 6

function mainLoad(){

  //getPercBet(40, 0,  100);
  //getPercBet(40, 10, 100);

  sessionStorage.setItem("low-perc",   1.0);
  sessionStorage.setItem("hig-perc",   1.0);
  sessionStorage.setItem("low-count",  1.0);
  sessionStorage.setItem("high-count", 1.0);
  sessionStorage.setItem("freq-low",   1.0);
  sessionStorage.setItem("freq-high",  1.0);
  sessionStorage.setItem("cloud-data",  "");

  //alert(calcPercOf(25, 200));
  //console.log("Starting");
  json = getPtxt();
  //buildWordcloud();
  setSizes();
  trimCommon();

}

function pullPercs(){

  return {
    hig:sessionStorage.getItem("hig-perc"),
    low:sessionStorage.getItem("low-perc"),
  }

}

//Get plaintext
function getPtxt(){

  var ret;
  $.ajax({
    type: "POST",
    url:"/plaintext",
    async: false,
    success: function(dat){
      console.log("Raw:", dat);
      ret = dat;
    },
    error: function(a,b,c){
      console.log("ABC:", a,b,c);
    }
  });

  return ret;
}

//Pull in and return the word frequency data as json, turned into an object
function getFreqs(){
  var targpath = "/frequencyJson"
  retdat = ""

  $.ajax({
    async: false, //For testing purposes
    type: "POST",
    url: targpath,
    //data: sendObj,
    dataType: "json",
    contentType: 'application/json; charset=utf-8',

      success: function(dat){
      console.log("Worked: ", dat);
        //return dat;
        retdat = dat;
      },
      error: function(a,b,c){
        console.log("Sendoff FAIL:", b, c)
      },
  });

  sessionStorage.setItem("cloud-data", retdat);

  getFreqRange(retdat);

  ///SEE VALUES =====================================================================
  for(const val of retdat.Data){
    console.log("VALUE: ", val.Count);
  }


  /*
  Switching from passing around json objects to sttoring json data as string in session storage
  */
 //Stringify and store rec'd data
  freqStr = JSON.stringify(retdat);
  console.log("freqStr:", freqStr)
  sessionStorage.setItem("freq-dat", freqStr);

  return retdat

}

//Iterate over the freqJson and set high/low number count values
function getFreqRange(jsonDataObj){ //Input: {"Word": "whole", "Count": 1}

  let low = 0.0
  let hig = 0.
  //First iteration to find biggest
  for(const iter of jsonDataObj.Data){
    if(iter.Count > hig){
      hig = iter.Count;
    }
  }

  //Now, to count back down to find smallest
  low = hig;

  //Start at the newly found biggest, then look for the lowest down from that 
  for(const currIter of jsonDataObj.Data){
    if(currIter.Count < low){
      low = currIter.Count;
    }
  }

  console.log("Set high/low:", hig, low);
  sessionStorage.setItem("freq-low", low);
  sessionStorage.setItem("freq-high", hig);

  return

}

//Return an object with the high/low range found by getFreqRange()
function pullRange(){

  return {
    low:sessionStorage.getItem("freq-low"),
    hig:sessionStorage.getItem("freq-hig"),
  };

}

//Send the entered data to the server
function sendoff(){
  tstp = Date.now();
  uname = document.getElementById("unameInput").value;
  paswd = document.getElementById("paswdInput").value;
  testim = document.getElementById("textarr").value;
  scaleval = document.getElementById("scaleval").value;
  scv = parseInt(scaleval);

  console.log("Uname: ", uname, "Password:", paswd)
  console.log("Type of scv", typeof(scaleval))

  //testim = cleanTest(testim)
  obj = {
    Timestamp: tstp,//.toString(),
    Uname: uname,
    Passwd: paswd,
    Testimonial: testim,
    Scale: scv,
  }

  sendit = JSON.stringify(obj);
  console.log("Sendit: ", sendit);

  
  $.ajax({
    async: false, //For testing purposes
    type: "POST",
    url: "/lstn",
    data: sendit,
    //data: sendObj,
    dataType: "json",
    //contentType: "application/json;charset=utf-8",
    contentType: 'application/json; charset=utf-8',
    /*
    success: function(result){//handleSuccess(response),
      console.log("res:",result)
    },
      */
      success: function(){
      console.log("Worked");
      },
      error: function(a,b,c){
        console.log("Sendoff FAIL:", b, c)
      },
  });
}
//========================================================================================
//Build the wordcloud=====================================================================
//========================================================================================
function buildWordcloud(){
  //alert("BWC");

  getFreqs();

  //let jsonData = getFreqs();
  let jsonData = sessionStorage.getItem("cloud-data");
  if(jsonData == ""){
    console.log("freq BLANK DATA");
  }else{
    console.log("freq GOT CLOUD DATA");
  }

  console.log("JSON DATA:", jsonData);

  jsonData = trimCloudData(jsonData);

  var targDiv  = document.getElementById("cloud");
  targDiv.innerHTML = "<h3 id=\"explan\">Click the Words Below for More Detail:</h3><hr id=\"innerspan\">";

  //Generates a pair of numbers to use in generating bucket classes and saves them to session storage
  genSizeRange(jsonData)
  buckets = 4

  for(i = 0; i < jsonData.Data.length; i++){  
    toInclude = jsonData.Data[i];

    generated = genDiv(toInclude);
    tdold = targDiv.innerHTML;
    targDiv.innerHTML = tdold + generated;
    
  }
}

//Clean out cloud data not between high and =low perc values
function trimCloudData(input){
  highLowCount = pullRange();
  highLowPercs = pullPercs();
  retMap = new Map();

  console.log("INPUT IS:", input);

  for(const iter of input.Data){
    perc = getPercBet(iter.Count,highLowCount.low, highLowCount.hig);

    //If we're within range...
    if(perc < highLowPercs.hig || perc > highLowPercs.low){
      retMap.add(iter.word, iter);
    }
  }

  return retMap

}

//Build the div that contains the lines of plaintext to display
//This is what puts the words in the wordcloud
function genDiv(iterObjt){

  customLab = "data-size-class=\"" + iterObjt["Count"] + "\"";
  classLabl = "class=\"wordspan _" +  iterObjt["Count"] + "\"";
  idLabel   = "id=\""+iterObjt["Word"]+"\"";
  //hyperLink = "<a href=\"/grep/"+iterObjt["Word"]+"\"\>"
  hyperLink=""
  oClick = "onclick=\"popModal(this); genLines('"+iterObjt["Word"]+"')\""
  divTotal  ="<span "+customLab + " " + oClick + classLabl + idLabel + ">" + hyperLink + iterObjt["Word"] + "</span>";

  //console.log("Div tot:", divTotal);
  return divTotal;  

}

//wordObj is the number of times the word is used 
//Return an array of size values for buckets. Something like: [5,10,15] for 5,10,and15 percent css values
function genSizeBuckets(wordObj, sizeRange, bucketCount){
  ret = []

  low = sizeRange.low;
  high = sizeRange.high;
  bucketSize = (high-low) / bucketCount

  div = Math.round( wordObj / bucketSize)

  ret.push(div)

  return ret
}

//Return what percent of 'big' 'small' is
function calcPercOf(small, big){

  console.log("types:", typeof(big), typeof(small));

  if(typeof(small) == typeof("") || typeof(big) == typeof("")){
    console.log("TYPEFAIL:",typeof(small),typeof(big));
    small = parseInt(small);
    big = parseInt(big);
  }

  if(small == 0 || big == 0){
    console.log("Zero in calcPercOf", small, big)
  }
  return (small / big) * 100
}

function genSizeRange(wordObj) {

  //Set html size variables

  console.log("GenSizeRange input:", wordObj)
  console.log("GS Data:", wordObj.Data)
  //Find the lowest "count" value
  low=3;
  for(i=0;i<wordObj.Data.length; i++){
    if (wordObj.Data[i].Count < low){
      console.log("Low:", low)
      console.log("cval for wordObj:", wordObj.Data[i])
      low = wordObj.Data[i].Count;
    }
    console.log("Data: ", wordObj.Data[i])
  }

  //Find the highest "count" 'value
  high=0;
  for(i=0;i<wordObj.Data.length; i++){
    if (wordObj.Data[i].Count > high){
      high = parseInt(wordObj.Data[i].Count);
    }
  }

  console.log("H&L:", high, low);

  //Set high low count
  sessionStorage.setItem("low-count",  low);
  sessionStorage.setItem("high-count", high);

  return //{low,high}
}

//Set the sizes of the freq words
function setSizes(){
  console.log("Starting setSizes");
  sizeElems = document.getElementsByClassName("wordspan"); 
  console.log("SizeElems: ", sizeElems);

  //Loop over the words and set css
  for(i=0; i <sizeElems.length;i++){

    dataSize = parseFloat(sizeElems[i].getAttribute("data-size-class"));
    sizePercStr = calcPercOf(dataSize, parseInt(sessionStorage.getItem("high-count")));
    matthThing = (Math.round(sizePercStr)/100).toString();
    sizeElems[i].style.fontSizeAdjust=matthThing;
    console.log("MT:", matthThing);
    
  }
}

//Ajax call to /grep/ to get json array of plaintext lines to display
function genLines(targword){

  wCloud = document.getElementById("cloud");
  cloudCont = wCloud.innerHTML;

  ret = "";
  targdir = "/grep/"+targword.value;

  console.log("Targdir: ", targdir);

  $.ajax({
    type: "POST",
    url:"/grep/"+targword,
    async: false,
    success: function(dat){
      console.log("Raw:", dat);
      ret = dat;
    },
    error: function(a,b,c){
      console.log("ABC:", a,b,c);
    }
  });

  console.log("Data: ", ret);
  addLines("", ret);
  //wCloud.innerHTML = addLines(cloudCont, ret);
}

//Takes cloud display innerhtml, formats toAdd, and tags toAdd to the bottom
function addLines(already, toAdd){

  if(toAdd == null){
    console.log("WARNING: ","toAdd is null! -- \nLine 383 js.js");
  }

  console.log("TO ADD:", toAdd);

  buildme = "";
  nob = JSON.parse(toAdd);
  console.log("NOB: ", nob);
  if(nob.length < 1){
    return "---Nob len bad---";
  }

  targDiv = document.getElementById("linecont");
  //console.log("noblen: ", nob.Data.length);

  targDiv.innerHTML = "<ul>"

  for(i=0; i < nob.Data.length; i++){

    console.log("ADDING: ", nob[i]);
    buildme = buildme+"<li><span class=\"entry_uname\">"+nob.Data[i].Username+":</span> <span class=\"statement\">"+nob.Data[i].Text+"</li>"

  }

  targDiv.innerHTML = targDiv.innerHTML + buildme + "<ul>";

  console.log("buildme: ", buildme);

  console.log("Fin addLines")
}





/*////////////////////////////////////////
*                                      ///
*                                      ///
* This page intentionally left blank   ///
*                                      ///
*                                      ///
*/////////////////////////////////////////


//For the user to cut off the top or bottom n% of the dataset
function updateRangeValue(slider){

  //Build a hashmap of words and their sizes
  words=new Map();
  smallest=0;
  biggest=0;

  if(slider.value < 50){
    sessionStorage.setItem("low-perc", parseFloat(slider.value));
  }else if(slider.value > 49){
    sessionStorage.setItem("hig-perc", parseFloat(slider.value));
  }else{
    console.log("BAD SLIDER VALUE");
  }

  _scrapeWords();
}

//Get slider percent ranges
function getPercRange(){

  return {
    low:sessionStorage.getItem("low-perc"),
    hig:sessionStorage.getItem("hig-perc"),
  }

}
//Go through wordcloud and then edit out the stuff that doesn't fall within user-defined ranges
function scrapeWords(){

  wcElems = document.getElementsByClassName("wordspan");
  cloudObj = JSON.parse(sessionStorage.getItem("freq-dat"))
  console.log("CloudObj type:", typeof(cloudObj));
  if(wcElems.length < 1){
    alert("WCELEMS ZERO LEN");
  }

  for(const elem of wcElems){
    console.log("ELEM: ", elem);
  }
}

//Actually do the scraping
function _scrapeWords(){

  document.getElementById("console").innerHTML = "";

  console.log("STARTING SCRAPEWORDS FUNCTION");

  const low = sessionStorage.getItem("low-perc");
  const hig = sessionStorage.getItem("hig-perc");
  const wcElems = document.getElementsByClassName("wordspan");
  var   words = new Map();
  var   finMap = new Map();
  var   wordsCount = 0;
  var   finMapCount = 0;

  for(const elem of wcElems){
    words.set(elem.id, elem);
  }
   
  for(const [key, value] of words){

    let sc = value.dataset.sizeClass

    console.log("GPB:",sc,low,hig);
    let perc = getPercBet(sc, low, hig);

    //Make sure the word is within range
    console.log("Perc v Low:", perc, low);
    if(perc > low && perc < hig){
      console.log("compare loop perc:",perc,"low:",low,"high:",hig)
      finMap.set(key, value);
    }else{
      console.log("Didn't make cut:", perc,low,high);
    }

    wordsCount++;
  }

  document.getElementById("console").innerHTML = "<ul>";

  for(const [key,value] of finMap){
    var cons = document.getElementById("console");
    let conval = cons.innerHTML;

    conval = conval+"<li>"+key+"</li>";
    cons.innerHTML = conval;
    console.log("KEY:",value);
    finMapCount++;
  };

  //Populate the "console" with the words we're going to be updating the wordcloud with.
  document.getElementById("console").innerHTML = document.getElementById("console").innerHTML + "</ul>";
  console.log("FINMAP LEN:", finMapCount);

  sessionStorage.setItem("cloud-data", finMap);

}

//Best function yet to get what percent from low to high val is
function getPercBet(val, low, high){

  if(val == 0 || low == 0 || high == 0){
    console.log("One equal zero:", val, low, high);
    if(low == 0){
      low = 1;
    }
    if(high==0){
      high=2;
    }
  }

  let res=(val-low)/(high-low)*100;
  console.log("Res is", res, "percent from",low,"to",high);
  return res
}


//Return the percentage of the range of sizes that 'val' is.
function getPercRange(val){

  const lowcnt = sessionStorage.getItem("low-count");
  const higcnt = sessionStorage.getItem("high-count");
  const range = higcnt - lowcnt;

  const loper = sessionStorage.getItem("low-perc");
  const hiper = sessionStorage.getItem("hig-perc");

  let fin = calcPercOf(parseInt(val), parseInt(range));
  console.log("PERC IS",fin,"PERCENT OF",val, "out of",range);

  //console.log("fin:", fin);
  return fin;
}


/*
** This is a monolithic attempt at what I've been using several different
** functions for. The attempt here is to simplify the process down and do it 
** in one function until it needs to be more complex.
** 
** Steps are:
** 1. Get slider values
** 2. Get the totalwordcloud data storage
** 3. Use slider values to trim the extremes of the wc data as specified by sliders
** 4. Rebuild the wordcloud
*/
function trimCommon(){
  //let bottomSliderValue = document.getElementById("freqRangeTop").value;
  //let topSlidervalue = document.getElementById("freqRangeBottom").value;

  let bottomSliderValue = document.getElementById("highEntry").value;
  let topSlidervalue    = document.getElementById("lowEntry" ).value;

  var retdat; //Data that we pull from the server
  var max = 0;
  var min = 100000000; //This needs to just be whatever the max is after finding it later. 
  var finArr = [];
  var cloud = document.getElementById("cloud");

  $.ajax({
    async: false, //For testing purposes
    type: "POST",
    url: "/frequencyJson",
    //data: sendObj,
    dataType: "json",
    contentType: 'application/json; charset=utf-8',

      success: function(dat){
      console.log("Worked: ", dat);
        //return dat;
        retdat = dat;
      },
      error: function(a,b,c){
        console.log("Sendoff FAIL:", b, c)
        //retdat = "FAIL";
      },
  });

  //Get max and min
  for(const curr of retdat.Data){
    if(curr.Count > max){
      max=curr.Count;
    }
  }

  for(const curr of retdat.Data){
    if(curr.Count < min){
      min=curr.Count;
    }
  }

  console.log("END: Max:", max, "Min:", min);
  console.log("END: BSV",bottomSliderValue);
  console.log("END: TSV",topSlidervalue);

  console.log("END: StarArr:", retdat.Data);

  for(const elm of retdat.Data){
    //Get percentile that elm is between max and min
    let perc = (elm.Count - min) / (max - min) * 100

    //If elm's percentile is higher than max or lower than min percentile, don't add it to the final array
    if(perc < bottomSliderValue){
      console.log("END: ELM:",perc, "is smaller than", bottomSliderValue, "||", elm);
      continue;
    }else if(perc > topSlidervalue){
      console.log("END: ELM:",perc, "is larger than", topSlidervalue, "||",elm);
      continue;
    }else{
      finArr.push(elm);
      console.log("END: ELM:", elm, "was added to the final list.");
    }

    
    //Add to what wordcloud html


  }

  cloud.innerHTML = "<h3 id=\"explan\">Click the Words Below for More Detail:</h3><hr id=\"innerspan\">";
  console.log("FinArr:",finArr);
  for(const elem of finArr){

    cloud.innerHTML = cloud.innerHTML + genDiv(elem);
    console.log("FINDAT:", cloud.innerHTML);

  }

  setSizes();

}
 
function popModal(that){
  window.scrollTo(0,0);
  //alert(that.id);
  document.getElementById("modalPop").style.display = "block";
  document.getElementById("modalPop").style.width = "100%";
  document.getElementById("modalPop").style.height = "100%";

  document.getElementById("inModal").innerHTML = document.getElementById("inModal").innerHTML + "<h1>"+that.id+"</h1>";
}

function deflateModal(){
  document.getElementById("modalPop").style.display = "none";
}