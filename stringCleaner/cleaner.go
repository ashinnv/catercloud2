package main

import (
	"database/sql"
	"fmt"
	"strings"
	"time"

	_ "github.com/go-sql-driver/mysql"
)

func main() {

	//rdr := bufio.NewReader(os.Stdin)

	for {
		lines := pullAllLines()
		lines = cleanLines(lines)
		putAllLines(lines)
		/*
			txt, _ := rdr.ReadString('\n')
			if txt == "q" {
				os.Exit(0)
				break
			}
		*/
		time.Sleep(5 * time.Second)

	}
}

func putAllLines(input ptxCont) {

	db, dbErr := sql.Open("mysql", "root:313231a@tcp(localhost:3306)/capperCloud")
	if dbErr != nil {
		fmt.Println("DB Err: ", dbErr)
	}
	defer db.Close()

	for _, ob := range input.Data {

		fmt.Println("Looping====================================")

		//If the entry doesn't exist, create it
		prepString := "INSERT INTO cleanText (id, text) VALUES (?,?)"
		fmt.Println("Prepstring: ", prepString)
		xec, xecErr := db.Prepare(prepString)
		if xecErr != nil {
			/*panic*/ fmt.Println("ERROR WITH PREPARE: " + xecErr.Error())
			time.Sleep(1 * time.Second)
			continue
		}

		//Adding a whitespace to help with searching for strings with a first character that matches.
		_, exerr := xec.Exec(ob.Id, ob.Text)
		if exerr != nil {
			fmt.Println("ERROR WRITING TO DATABASE: ", exerr)
		} else {
			fmt.Println("GOOD WRITE TO DB")
		}

		/*
			var exists bool
			querTxt := fmt.Sprintf("SELECT EXISTS(SELECT %d FROM cleanText);", ob.Id)
			row := db.QueryRow(querTxt)
			if scerr := row.Scan(&exists); scerr != nil {
				fmt.Println("WOopsIe bad DoinG oN Scan:", scerr)
			} else if !exists {
				fmt.Println("In exists: ", exists)
				fmt.Println("OB: ", ob)

				//If the entry doesn't exist, create it
				xec, xecErr := db.Prepare("INSERT INTO cleanText (id, text), VALUES (?,?)")
				if xecErr != nil {
					panic("Bad error with prepare: " + xecErr.Error())
				}

				//Adding a whitespace to help with searching for strings with a first character that matches.
				_, exerr := xec.Exec(ob.Id, ob.Text)
				if exerr != nil {
					fmt.Println("Error writing to database: ", exerr)
				} else {
					fmt.Println("Good write to db")
				}

			} else if exists {
				println("====================================")
				fmt.Println("Does exists: ", exists, ob.Id, ob.Text)
				fmt.Println("OB: ", ob)
				///*
					//Alter table if it does exist
					xec, xecErr := db.Prepare("UPDATE cleanText SET text=? where id=?")
					if xecErr != nil {
						fmt.Println("Error: ", xecErr)
						panic("Exec error: " + xecErr.Error())
					}

					_, execErr := xec.Exec(ob.Id, ob.Text)
					if execErr != nil {
						fmt.Println("Exec error: ", execErr)
						panic("Exec error: " + execErr.Error())
					}
				//
				fmt.Println("Skipping")
			}
		*/
	}
}

/*
var exists bool
row := db.QueryRow("SELECT EXISTS(SELECT 1 FROM ...)")
if err := row.Scan(&exists); err != nil {
    return err
} else if !exists {
    if err := db.Exec("INSERT ..."); err != nil {
        return err
    }
}
*/

func cleanLines(input ptxCont) ptxCont {

	repchars := []string{",", ".", "!", "?", ":", ";", "'", "\"", "@", "#", "$", "%", "^", "&", "*", "(", ")"}
	for lineNo := range input.Data {

		tmpObjt := input.Data[lineNo]
		for _, repMe := range repchars {
			tmpObjt.Text = strings.Replace(tmpObjt.Text, repMe, "", -1)
			tmpObjt.Text = " " + tmpObjt.Text
		}

		input.Data[lineNo] = tmpObjt

	}

	return input

}

func pullAllLines() ptxCont {

	fmt.Println("Starting genPlaintext")
	db, err := sql.Open("mysql", "root:313231a@tcp(localhost:3306)/capperCloud")
	if err != nil {
		panic(err.Error())
	}
	defer db.Close()

	genQuery := fmt.Sprintf("SELECT id, timestamp, username, text, scale FROM plaintext") // where text includes %s", input)

	//Run the request
	rows, err := db.Query(genQuery) // ORDER BY id DESC LIMIT 1")
	if err != nil {
		panic(err.Error())
	}
	defer rows.Close()

	/*
	   type ptxt struct {
	   	Id        int
	   	Timestamp uint64
	   	Username  string
	   	Text      string
	   	Scale     int
	   }

	   type ptxCont struct {
	   	Data []ptxt
	   }

	*/

	var results ptxCont
	for rows.Next() {
		var p ptxt
		err := rows.Scan(&p.Id, &p.Timestamp, &p.Username, &p.Text, &p.Scale)
		if err != nil {
			panic(err.Error())
		}
		results.Add(p) // = append(results.Data, p)
		//fmt.Println(results)
	}

	for _, iter := range results.Data {
		fmt.Println(iter.Text)
	}

	if err := rows.Err(); err != nil {
		panic(err.Error())
	}

	return results

}
