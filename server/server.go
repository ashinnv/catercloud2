package main

import (
	"net/http"
	"time"
)

func main() {

	go runSvr()

	for {
		time.Sleep(10000 * time.Hour)
	}
}

func runSvr() {
	http.HandleFunc("/", genRoot())
	http.HandleFunc("/css", genCss())
	http.HandleFunc("/js", genJs())
	http.HandleFunc("/jq", genJq())
	http.HandleFunc("/plaintext", genPlaintext())
	http.HandleFunc("/frequencyJson", genFreq())
	http.HandleFunc("/imgres/", genImgRes())
	http.HandleFunc("/lstn", genListen())
	http.HandleFunc("/grep/", genLines())

	http.ListenAndServe(":8080", nil)
}
