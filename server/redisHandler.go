package main

import (
	"database/sql"
	"fmt"

	_ "github.com/go-sql-driver/mysql"
)

func writeToSql(uip userInput) {

	fmt.Println("Starting writeToSql")

	db, dbErr := sql.Open("mysql", "root:313231a@tcp(localhost:3306)/capperCloud")
	if dbErr != nil {
		fmt.Println("DB Err: ", dbErr)
	}
	defer db.Close()

	stmt, stErr := db.Prepare("INSERT INTO plaintext (timestamp,username,text,scale,password) VALUES (?,?,?,?,?)")
	if stErr != nil {
		fmt.Println("Error preparing statment:", stErr)
		panic("")
	}

	//Adding a whitespace to help with searching for strings with a first character that matches.
	_, exerr := stmt.Exec(uip.Timestamp, uip.Uname, " "+uip.Testimonial, uip.Scale, uip.Passwd)
	if exerr != nil {
		fmt.Println("Error writing to database: ", exerr)
	}

}
