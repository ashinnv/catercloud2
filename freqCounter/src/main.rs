/*nix-shell -p libiconv openssl pkgconfig*/

use mysql::*;
use mysql::prelude::*;

use std::{thread,time};

use std::collections::HashMap;

struct Word {
    word: String,
    count: i64,
}

#[derive(Debug, PartialEq, Eq)]
struct Entry{
    id: u64,
    timestamp: u64,
    password: String,
    username: String,
    text: String,
    scale: i32,
}


fn main() -> std::result::Result<(), Box<dyn std::error::Error>> {
    let mut all_words: Vec<String> = vec![];
    let mut tmp_store: Vec<String> = vec![];
    let mut plaintext_lines: Vec<String> = vec![];

    let mut loopcount: u64 = 0;

    loop{
        //List of all sentences used, back to back.

        //Build the pool
        let url = "mysql://root:313231a@localhost:3306/capperCloud";
        let pool = Pool::new(url)?;

        //Get conn and get the query's results as a Vec<Entry>
        let mut conn = pool.get_conn()?;
        let ent_lines = conn
            .query_map(
                "SELECT id,timestamp,password,username,text,scale from plaintext",|(id,timestamp, password, username, text, scale)| {
                    Entry { id, timestamp, password, username, text, scale }
                },
            )?;

        //ent_lines is a list of plaintext entries.
        //If what we have is new compared to what we already have. If plaintext lines len doesn't match ent_lines text, re-up the plaintext lines.
        //if !check_match(&ent_lines, &plaintext_lines){
        //if !check_strings_match(&ent_lines, &plaintext_lines){
        //if !check_vec_matches(&ent_lines, &plaintext_lines) || loopcount % 150 == 0{
            if 3==3{
            println!("Bad match.");
            let mut unq_words = HashMap::new();

            //
            for line in &ent_lines{

                println!("Line: {}", line.text);

                let tmp_val = &line.text;
                let spl: Vec<&str> = tmp_val.split(" ").collect();
                for sp in spl{
                    all_words.push(clean_text(sp));
                }
            }
            
            if (all_words.len() as usize) < 1{
                //panic!("FUCKING XERO LEN???");
                println!("Zero len.");
                thread::sleep(time::Duration::from_secs(5));
            }

            for line in &all_words{
                let checked = unq_words.entry(line.to_string()).or_insert(0);
                let sum = *checked+1;
                unq_words.insert(line.to_string(),sum);
            }


            let mut unq_words = replace_plurals(unq_words);


            //Now, write the values out to mysql:
            for (k,v) in &unq_words{
                write_to_sql_word(k,*v as i64);
                //println!("Wrote {} as {}", k,*v);
            }

            plaintext_lines.clear();
            unq_words.clear();
            all_words.clear();
        }else{
            println!("Matched.");

            thread::sleep(time::Duration::from_secs(5));

        }


        if 3 != 3{
            break;
        } 

        println!("Loop Count: {}", loopcount);
        loopcount += 1;

    }

        Ok(())
    
}

//TODO: Make this check more robust.
//Check_match checks a vector of strings and a vector of Entry{}s and makes sure each string is accounted for by each Entry{}'s text.
fn check_match(ent_lines: &Vec<Entry>, ptl: &Vec<String>) -> bool {

    if ptl.len() != ent_lines.len(){
        false
    }else{
        true
    }
}

//Move to using this instead of check_match
fn check_strings_match( vec_of_structs: &[Entry], vec_of_strings: &[String]) -> bool {
    for my_struct in vec_of_structs {
        if !vec_of_strings.contains(&my_struct.text) {
            return false;
        }
    }
    true
}

fn check_vec_matches(entvec: &[Entry],strvec: &[String]) -> bool {

    for s in strvec {
        let found = entvec.iter().any(|ms| ms.text == *s);
        if found {
            break;
        } else {
            return false;
        }
    }
    return true;
}
//Erases non-alphanumeric characters  
fn clean_text(input: &str) -> String{
    let mut tmp_str = &input;
    let tmp_str = &tmp_str.replace(";","");
    let tmp_str = &tmp_str.replace(",","");
    let tmp_str = &tmp_str.replace("?","");
    let tmp_str = &tmp_str.replace("!", "");
    let tmp_str = &tmp_str.replace(":", "");
    let tmp_str = &tmp_str.replace(".","");

    tmp_str.to_lowercase()
}

fn write_to_sql_word(key: &str, val: i64){
    //println!("Setting {} as {}. ", key, val);

    let url = "mysql://root:313231a@localhost:3306/capperCloud";
    let pool = Pool::new(url).unwrap();

    let mut conn = pool.get_conn().unwrap();
    
    let tmp_wrd = Word{
        word: key.to_string(),
        count: val,
    };

    let puts = vec![
        Word{word: key.to_string(),count: val},
    ];
    
    if let Err(errr) = conn.exec_batch(
        r"INSERT INTO frequencyJson (word,count) VALUES (:word,:count)",puts.iter().map(|w| params!{
            "word"=> &w.word,
            "count"=> &w.count,
        })
    ){


        //println!("couldn't create mew entry: Key {} Val {}", key, val);

        if let Err(errrr) = conn.exec_batch(
            r"UPDATE frequencyJson SET count = :count WHERE word = :word",puts.iter().map(|w| params!{
                "word"=> &w.word,
                "count"=> &w.count,
            })
        ){
            println!("Didn't work: {}", errrr);
        }else{
            //println!("Did work.");
        }

    }else{
        //println!("Did do good.");
    }

}
/*nix-shell -p libiconv openssl pkgconfig*/

fn replace_plurals(mut input: HashMap<String, i32>) -> HashMap<String, i32>{

    for (key, value) in input.clone() {
        if key.ends_with("s") {
            let singular_key = key.trim_end_matches('s').to_string();
            if let Some(singular_value) = input.get(&singular_key) {
                input.insert(singular_key, singular_value + value);
                input.remove(&key);
            }
        }
    }

    return input
}

/*
fn replace_plurals(mut input: HashMap<String, i32>) -> (HashMap<String, i32>, Vec<String>){
    
    let mut plurals_to_remove: Vec<String> = vec![];

    //add plural entries to the singular versions.
    for (k1,v1) in input{
        for (k2,v2) in input{

            if k1.to_string() == k2.to_string() + "s"{
                let rval = &v1+&v2;
                &input.insert(k1.to_string(), rval);
                //input.remove(&k2.to_string());
            }
            plurals_to_remove.push(k2.to_string());
        }
    }

    //for plu in &plurals_to_remove{
    //    input.insert(plu.to_string(),0);
    //}

    return (input,plurals_to_remove)
}
*/
/*
fn append_line(ent: Entry, all_words: &mut Vec<String>) -> std::collections::HashMap<&str,i64>{
    let mut standin = &all_words;
    let mut inter_text = ent.text;
    let inter_text = inter_text.replace(".","");
    let spl: Vec<&str> = inter_text.split(" ").collect();

    //Put the individual worrds into the vector that we're sending back.
    for word in spl{
        all_words.push(word.to_string());
    }

    all_words
}
*/
